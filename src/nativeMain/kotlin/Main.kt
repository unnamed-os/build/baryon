import generator.ninja.ninja
import kotlinx.cinterop.toKString
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.required
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import buildables.Configuration
import buildables.Context
import buildables.Defaults
import buildables.Module
import buildables.TargetDevice
import okio.Path.Companion.toPath
import platform.posix.getenv

fun collectDevices(workingDir: String) =
    fs.listRecursively(workingDir.toPath(), false)
        .filter { it.name.endsWith(".device.json") }
        .onEach { println("Found device file $it") }
        .map { path ->
            runCatching {
                Json.decodeFromString<TargetDevice>(fs.read(path) { readByteString() }.utf8())
            }.onFailure { println("Failure processing $path") }.getOrThrow()
        }
        .toList()

fun collectConfigsInto(context: Context) {
    fs.listRecursively(context.baseDir, false)
        .filter { it.name.endsWith(".config.json") }
        .forEach { path ->
            Json.decodeFromString<Configuration>(fs.read(path) { readByteString() }.utf8()).apply {
                directory = path.normalized().parent!!
                definitionPath = path
                val allowedArches = arrayOf(
                    context.targetDevice.fullArch,
                    context.targetDevice.arch,
                ).distinct()
                if (arch == null || arch in allowedArches) {
                    println("Found config ${path.name} in $directory")
                    context.configs += this
                } else {
                    println("Defaults ${path.name} ($arch) in $directory is not of arches $allowedArches")
                }
            }
        }
}

fun collectDefaultsInto(context: Context) {
    fs.listRecursively(context.baseDir, false)
        .filter { it.name == "defaults.json" }
        .forEach { path ->
            Json.decodeFromString<List<Defaults>>(fs.read(path) { readByteString() }.utf8()).onEach { defaults ->
                defaults.directory = path.normalized().parent!!
                defaults.definitionPath = path
                val allowedArches = arrayOf(
                    context.targetDevice.fullArch,
                    context.targetDevice.arch,
                ).distinct()
                if (defaults.arch == null || defaults.arch in allowedArches) {
                    println("Found defaults ${defaults.name} in ${defaults.directory}")
                    context.defaults[defaults.name] = defaults.apply {
                        this.context = context
                        rules.allRules.forEach { it.module = this }
                    }
                } else {
                    println("Defaults ${defaults.name} (${defaults.arch}) in ${defaults.directory} " +
                            "is not of arches $allowedArches")
                }
            }
        }
}

fun collectModulesInto(context: Context) {
    fs.listRecursively(context.baseDir, false)
        .filter { it.name == "module.json" || it.name.endsWith(".module.json") }
        .forEach { path ->
            runCatching {
                Json.decodeFromString<Module>(fs.read(path) { readByteString() }.utf8()).apply {
                    directory = path.normalized().parent!!
                    moduleDefinitionPath = path
                }
            }.onFailure { println("Failure processing $path") }.getOrThrow().also { module ->
                context.modules[module.name]?.also { existingModule ->
                    println("Module duplicate found.")
                    println("Defined modules: ${context.modules.keys}")
                    error(
                        "Module ${module.name} in $path already defined in ${existingModule.moduleDefinitionPath}"
                    )
                } ?: run {
                    val allowedArches = arrayOf(
                        context.targetDevice.fullArch,
                        context.targetDevice.arch,
                    ).distinct()
                    if (module.arch == null || module.arch in allowedArches) {
                        println("Found module ${module.name} in ${module.directory}")
                        context.modules[module.name] = module.apply {
                            rules.allRules.forEach { it.module = this }
                            this.context = context
                        }
                    } else {
                        println("Module ${module.name} (${module.arch}) in ${module.directory} " +
                                "is not of arches: $allowedArches")
                    }
                }
            }
        }
}

fun parseArgs(parser: ArgParser, args: Array<String>) {
    if (args.isNotEmpty()) {
        parser.parse(args)
    } else {
        parser.parse((getenv("ARGV")?.toKString() ?: "").also {
            println("Using ARGV env as args: $it")
        }.split(" ").toTypedArray())
    }
}

fun main(args: Array<String>) {
    val parser = ArgParser("baryon")
    val workingDir by parser.option(ArgType.String, shortName = "d", description = "Working directory").default(".")
    val outputFile by parser.option(ArgType.String, shortName = "o", description = "Ninja build file output").required()
    val target by parser.option(ArgType.String, shortName = "t", description = "Target device").required()
    val outDir by parser.option(ArgType.String, description = "Output directory for build output").default("out")
    val mainRule by parser.option(
        ArgType.String, description = "Main rule to build. If empty, ninja decides").default("")
    parseArgs(parser, args)

    val devices = collectDevices(workingDir)

    val context = Context(
        outDir = outDir.toPath(),
        targetDevice = devices.find { it.name == target } ?:
            error("Unknown device $target. Available devices: ${devices.map { it.name }.toList()}"),
        baseDir = workingDir.toPath(true),
    )

    println("Base dir: ${context.baseDir}")

    collectConfigsInto(context)
    collectDefaultsInto(context)
    collectModulesInto(context)

    ninja({
        buildDir = outDir
        if (mainRule.isNotEmpty()) {
            defaults += mainRule
        }
    }) {
        context.modules.forEach { (_, module) ->
            +module.rules.ninjaRules(context)
        }
        context.modules.forEach { (_, module) ->
            +module.rules.ninjaInstructions(context)
        }
    }.toString().let { generated ->
        fs.write(outputFile.toPath()) {
            write(generated.encodeToByteArray())
        }
    }
}
