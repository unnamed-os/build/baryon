import kotlinx.cinterop.toKString
import okio.Path.Companion.toPath
import platform.posix.getenv

fun searchPath(name: String) =
    getenv("PATH")!!.toKString().split(":").firstNotNullOfOrNull { pathInPath ->
        fs.listOrNull(pathInPath.toPath())?.find { it.name == name }?.toString()
    }

fun findFirstInPath(vararg names: String) = names.firstNotNullOfOrNull { searchPath(it) }
