package lib.extensions

inline fun <reified T> List<T>.paddedBy(minSize: Int, padBy: (Int) -> T) =
    this + (size until minSize).map(padBy)

inline fun <reified T> List<T>.asPair() =
    if (size == 2) this[0] to this[1]
    else error("List must contain exactly 2 items")

inline fun String.splitPadded(
    vararg delimiters: String, max: Int, min: Int = max, padBy: (Int) -> String
) = split(*delimiters, limit = max).paddedBy(min, padBy)
