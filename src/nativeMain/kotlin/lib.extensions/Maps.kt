package lib.extensions

inline fun <reified K, reified V> Iterable<Map<K, V>>.flatten() = flatMap { it.entries }.associate { it.toPair() }

inline fun <reified K, reified V, reified T> Iterable<T>.mapFlattened(
    transform: (T) -> Map<K, V>
): Map<K, V> = map(transform).flatten()

