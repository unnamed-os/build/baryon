package toolchains.types

import buildables.TargetPlatform

interface SupportsLLVMIRCompiling : Toolchain {
    fun compileLLVMIR(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        optimizationLevel: String = "2",
        additionalFlags: List<String> = listOf(),
        depFile: String? = null,
    ): List<String>

    fun llvmIRFlagsFor(platform: TargetPlatform): List<String> = listOf()
}
