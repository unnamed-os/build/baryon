package toolchains.types

import buildables.TargetPlatform

interface SupportsRustCompiling : Toolchain {
    fun compileRust(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        additionalFlags: List<String>,
        optimizationLevel: String = "0",
        depFile: String? = null,
        edition: String = "2021",
        nightly: Boolean = false,
    ): List<String>

    fun rustFlagsFor(platform: TargetPlatform) = listOf<String>()
}