package toolchains.types

import buildables.TargetPlatform

interface SupportsCCompiling : Toolchain {
    fun compileC(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        optimizationLevel: String = "2",
        additionalFlags: List<String> = listOf(),
        depFile: String? = null,
        cStandard: String = "c17"
    ): List<String>

    fun cFlagsFor(platform: TargetPlatform): List<String> = listOf()
}