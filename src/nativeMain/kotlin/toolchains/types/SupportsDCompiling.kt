package toolchains.types

import buildables.TargetPlatform

interface SupportsDCompiling : Toolchain {
    fun compileD(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        optimizationLevel: String = "2",
        additionalFlags: List<String> = listOf(),
        depFile: String? = null,
    ): List<String>

    fun dFlagsFor(platform: TargetPlatform): List<String> = listOf()
}