package toolchains.types

import buildables.TargetPlatform

interface SupportsSwiftCompiling : Toolchain {
    fun compileSwift(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        optimize: Boolean = true,
        additionalFlags: List<String> = listOf(),
        swiftVersion: String = "5",
    ): List<String>

    fun swiftFlagsFor(platform: TargetPlatform): List<String> = listOf()
}
