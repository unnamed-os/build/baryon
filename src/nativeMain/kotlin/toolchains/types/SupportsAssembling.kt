package toolchains.types

import buildables.TargetPlatform

interface SupportsAssembling : Toolchain {
    fun assemble(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        additionalFlags: List<String> = listOf(),
        depFile: String? = null,
    ): List<String>

    fun asmFlagsFor(platform: TargetPlatform): List<String> = listOf()
}