package toolchains.types

import buildables.TargetPlatform

interface SupportsCPPCompiling : Toolchain {
    fun compileCPP(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        optimizationLevel: String = "2",
        additionalFlags: List<String> = listOf(),
        depFile: String? = null,
        cppStandard: String = "c++17"
    ): List<String>

    fun cppFlagsFor(platform: TargetPlatform): List<String> = listOf()
}