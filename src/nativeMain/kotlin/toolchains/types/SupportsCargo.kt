package toolchains.types

import buildables.shellEscaped

interface SupportsCargo : Toolchain, SupportsRustCompiling {
    fun runCargo(
        parameters: List<String>,
        configs: Map<String, String> = mapOf(),
        nightly: Boolean = false,
    ): List<String>

    fun cargoBuild(
        targetTriple: TargetTriple,
        targetDir: String? = null,
        manifestPath: String? = null,
        pkg: String? = null,
        profile: String = "release",
        additionalParameters: List<String> = listOf(),
        configs: Map<String, String> = mapOf(),
        unstableOptions: Map<String, String?> = mapOf(),
        rustFlags: List<String> = listOf(),
        jobs: Int = 1,
        nightly: Boolean = false,
    ) = runCargo(
        parameters = listOf("build") +
                unstableOptions
                    .flatMap { (k, v) -> listOf("-Z", v?.ifEmpty { null }?.let { "$k=$v" } ?: k) }
                    .map { it.shellEscaped } +
                additionalParameters +
                listOf(
                    "--target", targetTriple.toString(),
                    "--jobs", jobs.toString(),
                    "--profile", profile,
                    *manifestPath?.let { arrayOf("--manifest-path", it) } ?: arrayOf(),
                    *targetDir?.let { arrayOf("--target-dir", it) } ?: arrayOf(),
                    *pkg?.let { arrayOf("-p", it) } ?: arrayOf(),
                ),
        configs = configs + mapOf("build.rustflags" to "\"${rustFlags.joinToString(" ")}\""),
        nightly = nightly,
    )
}