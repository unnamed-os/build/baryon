package toolchains.types

import kotlinx.serialization.Serializable

@Serializable
data class TargetTriple(
    val arch: String,
    val sub: String? = null,
    val vendor: String? = null,
    val sys: String? = null,
    val env: String? = null,
) {
    override fun toString() =
        listOfNotNull(fullArch, vendor, sys, env).joinToString("-")

    val fullArch get() = listOfNotNull(arch, sub).joinToString("")
}
