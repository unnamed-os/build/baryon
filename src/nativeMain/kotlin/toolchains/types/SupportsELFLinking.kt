package toolchains.types

import buildables.TargetPlatform

interface SupportsELFLinking : Toolchain {
    fun link(
        inputFiles: List<String>,
        outputFile: String,
        targetTriple: TargetTriple,
        optimizationLevel: String = "2",
        additionalFlags: List<String> = listOf(),
        linkerScript: String? = null,
        static: Boolean = true,
        useLD: String? = null,
    ): List<String>

    fun ldFlagsFor(platform: TargetPlatform): List<String> = listOf()
}
