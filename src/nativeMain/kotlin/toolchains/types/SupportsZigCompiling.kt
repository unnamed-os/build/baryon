package toolchains.types

import buildables.TargetPlatform

interface SupportsZigCompiling : Toolchain {
    fun compileZig(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String> = listOf(),
        optimizationLevel: String = "ReleaseFast",
        additionalFlags: List<String> = listOf(),
    ): List<String>

    fun zigFlagsFor(platform: TargetPlatform): List<String> = listOf()
}
