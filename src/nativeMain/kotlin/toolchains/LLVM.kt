package toolchains

import findFirstInPath
import buildables.TargetPlatform
import toolchains.types.SupportsAssembling
import toolchains.types.SupportsCCompiling
import toolchains.types.SupportsCPPCompiling
import toolchains.types.SupportsELFLinking
import toolchains.types.SupportsLLVMIRCompiling
import toolchains.types.TargetTriple
import toolchains.types.Toolchain

object LLVM :
    SupportsCCompiling,
    SupportsCPPCompiling,
    SupportsELFLinking,
    SupportsAssembling,
    SupportsLLVMIRCompiling,
    Toolchain
{

    private val llvmVersionRange = 16 downTo 15
    val clangCommand by lazy {
        findFirstInPath(*llvmVersionRange.map { "clang-$it" }.toTypedArray(), "clang")
            ?: error("clang not found in path. Install one of ${llvmVersionRange.toList()}")
    }
    val clangPPCommand by lazy {
        findFirstInPath(
            *llvmVersionRange.map { "clang++-$it" }.toTypedArray(),
            *llvmVersionRange.map { "clang-$it" }.toTypedArray(),
            "clang", "clang++"
        ) ?: error("clang++ not found in path. Install one of ${llvmVersionRange.toList()}")
    }
    val lldCommand by lazy {
        findFirstInPath(*llvmVersionRange.map { "ld.lld-$it" }.toTypedArray(), "ld.lld")
            ?: error("lld not found in path. Install one of ${llvmVersionRange.toList()}")
    }
    val assembleCommand by lazy { clangCommand }

    override fun compileC(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        optimizationLevel: String,
        additionalFlags: List<String>,
        depFile: String?,
        cStandard: String
    ) = listOfNotNull(
        clangCommand,
        "-target",
        targetTriple.toString(),
        *additionalFlags.toTypedArray(),
        "-std=$cStandard",
        *includeDirectories.map { "-I$it" }.toTypedArray(),
        "-O$optimizationLevel",
        *(depFile?.let { arrayOf("-MD", "-MF", it) } ?: arrayOf()),
        "-c", sourceFile,
        "-o", outputFile
    )

    override fun compileCPP(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        optimizationLevel: String,
        additionalFlags: List<String>,
        depFile: String?,
        cppStandard: String
    ) = listOfNotNull(
        clangPPCommand,
        "-target",
        targetTriple.toString(),
        *additionalFlags.toTypedArray(),
        "-std=$cppStandard",
        *includeDirectories.map { "-I$it" }.toTypedArray(),
        "-O$optimizationLevel",
        *(depFile?.let { arrayOf("-MD", "-MF", it) } ?: arrayOf()),
        "-c", sourceFile,
        "-o", outputFile
    )

    override fun link(
        inputFiles: List<String>,
        outputFile: String,
        targetTriple: TargetTriple,
        optimizationLevel: String,
        additionalFlags: List<String>,
        linkerScript: String?,
        static: Boolean,
        useLD: String?
    ) = listOfNotNull(
        useLD?.let { clangCommand } ?: lldCommand,
        "-target",
        targetTriple.toString(),
        if (static) "-static" else null,
        *additionalFlags.toTypedArray(),
        "-O$optimizationLevel",
        *(linkerScript?.let { arrayOf("-T", linkerScript) } ?: arrayOf() ),
        useLD?.let { "-fuse-ld=$it" },
        *inputFiles.toTypedArray(),
        "-o",
        outputFile
    )

    override fun assemble(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        additionalFlags: List<String>,
        depFile: String?,
    ) = listOfNotNull(
        assembleCommand,
        "-target",
        targetTriple.toString(),
        *additionalFlags.toTypedArray(),
        *includeDirectories.map { "-I$it" }.toTypedArray(),
        *(depFile?.let { arrayOf("-MD", "-MF", it) } ?: arrayOf()),
        "-c", sourceFile,
        "-o", outputFile
    )

    override fun cFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-ffreestanding", "-fno-builtin", "-nostdlib",
                "-mno-red-zone", "-fPIC"
            )
        }

    override fun cppFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-ffreestanding", "-fno-builtin", "-nostdlib",
                "-mno-red-zone", "-fPIC"
            )
        }

    override fun ldFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-ffreestanding", "-nodefaultlibs", "-nostdlib",
                "-fPIC"
            )
        }

    override fun asmFlagsFor(platform: TargetPlatform) = listOf<String>()

    override fun compileLLVMIR(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        optimizationLevel: String,
        additionalFlags: List<String>,
        depFile: String?,
    ) = listOfNotNull(
        clangCommand,
        "-target",
        targetTriple.toString(),
        *additionalFlags.toTypedArray(),
        "-O$optimizationLevel",
        *(depFile?.let { arrayOf("-MD", "-MF", it) } ?: arrayOf()),
        "-c", sourceFile,
        "-o", outputFile
    )

    override fun llvmIRFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-ffreestanding", "-fno-builtin", "-mno-red-zone", "-fPIC"
            )
        }
}