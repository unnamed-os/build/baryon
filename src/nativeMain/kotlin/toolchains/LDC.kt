package toolchains

import findFirstInPath
import buildables.TargetPlatform
import toolchains.types.SupportsDCompiling
import toolchains.types.TargetTriple
import toolchains.types.Toolchain

object LDC : SupportsDCompiling, Toolchain {

    val ldcCommand by lazy {
        findFirstInPath("ldc", "ldc2")
            ?: error("ldc not found in path. Install the LLVM D Compiler")
    }

    override fun compileD(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        optimizationLevel: String,
        additionalFlags: List<String>,
        depFile: String?,
    ) = listOfNotNull(
        ldcCommand,
        "--mtriple",
        targetTriple.toString(),
        *additionalFlags.toTypedArray(),
        *includeDirectories.map { "-I$it" }.toTypedArray(),
        "-O$optimizationLevel",
        depFile?.let { "--makedeps=$it" },
        "--output-o",
        "-c", sourceFile,
        "--of=$outputFile"
    )

    override fun dFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-nodefaultlib",
                "--betterC", "--checkaction=halt",
                "--nogc", "--relocation-model=pic",
            )
        }
}