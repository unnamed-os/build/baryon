package toolchains

import buildables.TargetPlatform
import findFirstInPath
import toolchains.types.SupportsZigCompiling
import toolchains.types.TargetTriple
import toolchains.types.Toolchain

object ZigCompiler : SupportsZigCompiling, Toolchain {
    val zigCommand by lazy {
        findFirstInPath("zig")
            ?: error("zig not found in path. Install the Zig Compiler")
    }

    override fun compileZig(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        optimizationLevel: String,
        additionalFlags: List<String>,
    ) = listOfNotNull(
        zigCommand,
        "build-obj",
        "-O", optimizationLevel,
        "-target", targetTriple.toString(),
        "-ofmt=elf",
        "-fLLVM",
        *additionalFlags.toTypedArray(),
        *includeDirectories.map { "-I $it" }.toTypedArray(),
        "-fno-emit-bin",
        "-femit-llvm-ir=$outputFile",
        sourceFile,
    )

    override fun zigFlagsFor(platform: TargetPlatform) = when (platform) {
        TargetPlatform.BareMetal -> listOf(
            "-mno-red-zone", "-fPIC", "-fno-builtin",
        )
    }
}