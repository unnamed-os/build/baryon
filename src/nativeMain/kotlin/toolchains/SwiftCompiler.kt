package toolchains

import buildables.TargetPlatform
import findFirstInPath
import toolchains.types.SupportsSwiftCompiling
import toolchains.types.TargetTriple
import toolchains.types.Toolchain

object SwiftCompiler : SupportsSwiftCompiling, Toolchain {
    val swiftCommand by lazy {
        findFirstInPath("swiftc-58", "swiftc-5", "swiftc")
            ?: error("swiftc not found in path. Install the Swift Compiler")
    }

    override fun compileSwift(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        includeDirectories: List<String>,
        optimize: Boolean,
        additionalFlags: List<String>,
        swiftVersion: String,
    ) = listOfNotNull(
        swiftCommand,
        // For now, we don't specify a target because freestanding is not supported
        //"-target", targetTriple.toString(),
        "-parse-as-library",
        "-emit-library",
        "-emit-ir",
        "-emit-dependencies",
        "-warn-concurrency",
        "-warn-implicit-overrides",
        "-swift-version", swiftVersion,
        if (optimize) "-O" else null,
        *additionalFlags.toTypedArray(),
        *includeDirectories.map { "-I $it" }.toTypedArray(),
        sourceFile,
        "-o", outputFile,
    )

    override fun swiftFlagsFor(platform: TargetPlatform) = when (platform) {
        TargetPlatform.BareMetal -> listOf(
            "-disable-autolinking-runtime-compatibility-concurrency",
            "-disable-autolinking-runtime-compatibility-dynamic-replacements",
            "-disable-autolinking-runtime-compatibility",
            "-remove-runtime-asserts",
        )
    }
}