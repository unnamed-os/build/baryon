package toolchains

import buildables.TargetPlatform
import buildables.shellEscaped
import findFirstInPath
import toolchains.types.SupportsCargo
import toolchains.types.SupportsRustCompiling
import toolchains.types.TargetTriple
import toolchains.types.Toolchain

object RustToolchain : SupportsRustCompiling, SupportsCargo, Toolchain {
    val rustcCommand by lazy {
        findFirstInPath("rustc")
            ?: error("rustc not found in path. Install Rust")
    }
    val cargoCommand by lazy {
        findFirstInPath("cargo")
            ?: error("cargo not found in path. Install Rust")
    }

    private inline val String.isObject get() = endsWith(".o")
    private inline val String.emitType get() = if (isObject) "obj" else "link"

    override fun compileRust(
        sourceFile: String,
        outputFile: String,
        targetTriple: TargetTriple,
        additionalFlags: List<String>,
        optimizationLevel: String,
        depFile: String?,
        edition: String,
        nightly: Boolean,
    ) = listOfNotNull(
        rustcCommand,
        if (nightly) "+nightly" else null,
        "--target", targetTriple.toString(),
        "--crate-type=staticlib",
        "--edition", edition,
        "-C", "opt-level=$optimizationLevel",
        *additionalFlags.toTypedArray(),
        "--emit", listOfNotNull(
            "${sourceFile.emitType}=$outputFile",
            depFile?.let { "dep-info=$it" }
        ).joinToString(","),
        sourceFile
    )

    override fun rustFlagsFor(platform: TargetPlatform) =
        when (platform) {
            TargetPlatform.BareMetal -> listOf(
                "-C", "panic=abort",
                "-C", "relocation-model=pic",
            )
        }

    override fun runCargo(
        parameters: List<String>,
        configs: Map<String, String>,
        nightly: Boolean,
    ) =
        listOfNotNull(
            cargoCommand,
            if (nightly) "+nightly" else null,
        ) +
                configs.flatMap { (key, value) -> listOf("--config", "$key=$value".shellEscaped) } +
                parameters
}