package toolchains

import toolchains.types.Toolchain

inline fun <reified T : Toolchain> String.fromToolchains(tcMap: Map<String, T>) =
    tcMap[this] ?: error("Toolchain $this of type ${T::class.simpleName} not supported. Choose one of ${tcMap.keys}")
