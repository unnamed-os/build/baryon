import okio.FileSystem
import okio.Path
import okio.Path.Companion.toPath

val fs = FileSystem.SYSTEM
val Path.isDirectory get() = fs.metadataOrNull(this)?.isDirectory == true
val Path.isRegularFile get() = fs.metadataOrNull(this)?.isRegularFile == true

fun Path.replaceExtension(newExt: String) =
    "${parent?.toString()?.let { "$it/" } ?: ""}${name.split(".").let { splitPath ->
        if (splitPath.size == 1) "${splitPath.first()}.$newExt"
        else "${splitPath.dropLast(1).joinToString(".")}.$newExt"
    }}".toPath()

fun Path.startsWith(other: Path) = normalized().run {
    other.normalized().let { normalizedOther ->
        normalizedOther.segments.size <= segments.size &&
                segments
                    .slice(0 until normalizedOther.segments.size)
                    .filterIndexed { index, s -> normalizedOther.segments[index] != s }
                    .isEmpty()
    }
}
