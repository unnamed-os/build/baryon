package buildables

import kotlinx.serialization.Serializable
import toolchains.types.TargetTriple

@Serializable
data class TargetDevice(
    val name: String,
    val arch: String,
    val archSub: String? = null,
    val toolchains: Map<String, ToolchainConfig>
) {
    val fullArch get() = listOfNotNull(arch, archSub).joinToString("")
}

@Serializable
data class ToolchainConfig(
    val targetTriple: TargetTriple,
)
