package buildables

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import okio.Path

interface ModuleLike {
    var context: Context
    val directory: Path
    val name: String

    fun relativePath(path: Path) = path.relativeTo(directory)
}

@Serializable
data class Module(
    override val name: String,
    val arch: String? = null,
    val rules: Rules = Rules(),
): ModuleLike {
    @Transient
    override lateinit var context: Context

    @Transient
    override lateinit var directory: Path

    @Transient
    lateinit var moduleDefinitionPath: Path

    inline fun <reified T : Rule> inputFilesOf(): List<Path> =
        rules.ruleOfType<T>()?.inputFiles() ?: listOf()
}

// For later, when the toml library supports arrays of tables
@Serializable
data class Modules(
    @SerialName("module")
    val modules: List<Module> = listOf()
)

