package buildables.rules

import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.Serializable
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsSources
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveSrc
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.inputFilesCommon
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.asNinjaCommand
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.LLVM
import toolchains.types.SupportsAssembling

private val supportedToolchains = mapOf<String, SupportsAssembling>(
    "llvm" to LLVM,
)

@Serializable
data class ASM(
    override val src: List<String>? = null,
    override val platform: TargetPlatform? = null,
    val flags: List<String> = listOf(),
    val includes: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsPlatforms, SupportsToolchains, SupportsSources {

    private val effectivePlatform by delegatedEffectivePlatform()
    override val effectiveSrc by delegatedEffectiveSrc()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    private fun combinedFlags(): List<String> =
        (tc.asmFlagsFor(effectivePlatform) + flags + defaults<ASM>().flatMap { it.combinedFlags() }).distinct()

    private fun combinedIncludes(): List<String> =
        (includes + defaults<ASM>().flatMap { it.combinedIncludes() }).distinct()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [ASM] \$out",
            depFile = "\$out.d",
            command = tc.assemble(
                sourceFile = "\$in",
                outputFile = "\$out",
                depFile = "\$out.d",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                includeDirectories = combinedIncludes(),
                additionalFlags = combinedFlags(),
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = inputFilesCommon(".s")

    override fun ninjaInstructions() =
        inputFiles().map { cSourcePath ->
            NinjaBuildInstruction(
                outputs = listOf(
                    (outputDirFor(this@ASM) / module.relativePath(cSourcePath)).replaceExtension("o").toString()
                ),
                ruleName = ruleName,
                inputs = listOf(context.baseRelativePath(cSourcePath).toString())
            )
        }
}

