package buildables.rules

import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.context
import buildables.outputDirFor
import buildables.ruleName
import buildables.shellEscaped
import findFirstInPath
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.Serializable
import okio.Path
import okio.Path.Companion.toPath
import startsWith

@Serializable
data class Unzip(
    val file: String? = null,
    val fileFrom: String? = null,
    val extract: List<String> = listOf(),
    val intermediate: Boolean = false,
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults() {

    override val outType
        get() = if (intermediate) OutType.Intermediary else OutType.Product

    override fun inputFiles() =
        file?.toPath(normalize = true)?.let { listOf(it) }
            ?: fileFrom?.let { context.rules[fileFrom]?.outputFiles() ?: error("rule $fileFrom not found") }
            ?: error("Specify either file or fileFrom")

    override fun outputFiles() = extract.map { extractName ->
        (outputDirFor(this) / extractName).also {
            if (!it.normalized().startsWith(outputDirFor(this))) {
                error("file to extract $extractName is outside of allowed directory ${outputDirFor(this)}")
            }
        }
    }

    override fun ninjaRule() = NinjaRule(
        name = ruleName,
        description = "[unzip] \$in (\$out)",
        command = listOf(
            findFirstInPath("unzip") ?: error("unzip is required"),
            "-DD",
            "-o", "\$in", "-d", outputDirFor(this).toString().shellEscaped,
            "$$(${
                listOf(
                    findFirstInPath("realpath") ?: error("realpath is required (coreutils)"),
                    "--relative-to=${outputDirFor(this).toString().shellEscaped}",
                    "\$out"
                ).joinToString(" ")
            })"
        ).joinToString(" ")
    )

    override fun ninjaInstructions() = listOf(
        NinjaBuildInstruction(
            outputs = outputFiles().map { it.toString() },
            ruleName = ruleName,
            inputs = inputFiles().map { it.toString() },
        )
    )
}
