package buildables.rules

import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.outputDirFor
import buildables.ruleName
import buildables.shellEscaped
import findFirstInPath
import fs
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.Serializable
import okio.Path
import startsWith

@Serializable
data class Download(
    val url: String,
    val filename: String,
    val intermediate: Boolean = false,
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults() {
    override val outType
        get() = if (intermediate) OutType.Intermediary else OutType.Product

    override fun outputFiles() = listOf(
        (outputDirFor(this) / filename).also {
            if (!it.normalized().startsWith(outputDirFor(this))) {
                error("$filename is outside of allowed directory ${outputDirFor(this)}")
            }
        }
    )

    override fun ninjaRule() = NinjaRule(
        name = ruleName,
        description = "[download] $url (\$out)",
        command = listOf(
            findFirstInPath("curl") ?: error("curl is required"), "-fsL", "-o", "\$out", "$$(<\$in)"
        ).joinToString(" ")
    )

    override fun ninjaInstructions(): List<NinjaBuildInstruction> {
        // Generate a file in out that will be used as dependency
        // so that the file won't be redownloaded every time
        val implicitDepFile = (outputDirFor(this) / ".dep").also {
            if (!fs.exists(it)) {
                fs.createDirectories(it.parent!!, mustCreate = false)
                fs.write(it, mustCreate = true) { write(url.encodeToByteArray()) }
            } else {
                var needsRewrite = false
                fs.read(it) {
                    if (readByteString(url.length.toLong()).utf8() != url) {
                        needsRewrite = true
                    }
                }
                if (needsRewrite) {
                    fs.write(it, mustCreate = false) { write(url.encodeToByteArray()) }
                }
            }
        }
        return listOf(
            NinjaBuildInstruction(
                outputs = outputFiles().map { it.toString() },
                ruleName = ruleName,
                inputs = listOf(implicitDepFile.toString()),
            )
        )
    }
}
