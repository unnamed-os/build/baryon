package buildables.rules

import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsSources
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveSrc
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.inputFilesCommon
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import generator.ninja.asNinjaCommand
import kotlinx.serialization.Serializable
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.RustToolchain
import toolchains.types.SupportsRustCompiling
import kotlin.reflect.KProperty

private val supportedToolchains = mapOf<String, SupportsRustCompiling>(
    "rust" to RustToolchain,
)

enum class RustEmitType(val fileExt: String) {
    Object("o"), StaticLib("a"),
    RLib("rlib"), SharedLib("so")
    ;

    fun transformModuleName(name: String) = when (this) {
        StaticLib, SharedLib, RLib -> "lib${name.replace("-", "_")}"
        else -> name.replace("-", "_")
    }
}

@Serializable
data class Rust(
    override val src: List<String>? = null,
    override val platform: TargetPlatform? = null,
    val options: RustOptions = RustOptions(),
    val arch: Map<String, RustArchSpecific> = mapOf(),
    val flags: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    val emit: RustEmitType = RustEmitType.StaticLib,
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsToolchains, SupportsSources, SupportsPlatforms {

    private val effectivePlatform by delegatedEffectivePlatform()
    override val effectiveSrc by delegatedEffectiveSrc()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    override val outType = OutType.Intermediary

    private fun combinedFlags(): List<String> =
        (tc.rustFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.flags ?: listOf()) +
                (arch[context.targetDevice.arch]?.flags ?: listOf()) +
                options.unstable.flags +
                defaults<Rust>().flatMap { it.combinedFlags() })

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [Rust] \$out",
            depFile = "\$out.d",
            command = tc.compileRust(
                sourceFile = "\$in",
                outputFile = "\$out",
                depFile = "\$out.d",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                additionalFlags = combinedFlags(),
                edition = options.edition ?: defaults<Rust>().firstNotNullOfOrNull { it.options.edition } ?:
                    error("Please specify edition in Rust rule"),
                optimizationLevel = options.optimize ?: defaults<Rust>().firstNotNullOfOrNull { it.options.optimize } ?:
                    "0",
                nightly = options.nightly ?: defaults<Rust>().firstNotNullOfOrNull { it.options.nightly } ?: false,
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = inputFilesCommon(".rs")

    override fun ninjaInstructions() = listOf(
        NinjaBuildInstruction(
            outputs = listOf(
                (outputDirFor(this@Rust) / "$ruleName.${emit.fileExt}").toString()
            ),
            ruleName = ruleName,
            inputs = inputFiles().map { cSourcePath -> context.baseRelativePath(cSourcePath).toString() }
        )
    )
}

@Serializable
data class RustArchSpecific(
    val flags: List<String> = listOf()
)

@Serializable
data class RustOptions(
    val edition: String? = null,
    val optimize: String? = null,
    val nightly: Boolean? = null,
    val unstable: RustUnstableOptions = RustUnstableOptions(),
)

@Serializable
data class RustUnstableOptions(
    val other: List<String> = listOf()
) {
    private val mergedOptions by lazy {
        other
    }

    private val allOptions get() =
        if (mergedOptions.isEmpty()) listOf() else listOf("unstable-options") + mergedOptions


    val flags get() = allOptions.flatMap { listOf("-Z", it) }
}