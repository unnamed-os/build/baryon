package buildables.rules

import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.findInputFiles
import buildables.outputDirFor
import buildables.ruleName
import buildables.shellEscaped
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import generator.ninja.asNinjaCommand
import kotlinx.serialization.Serializable
import lib.extensions.asPair
import lib.extensions.mapFlattened
import lib.extensions.splitPadded
import okio.Path
import okio.Path.Companion.toPath
import toolchains.RustToolchain
import toolchains.types.SupportsCargo

private val supportedToolchains = mapOf<String, SupportsCargo>(
    "rust" to RustToolchain,
)

@Serializable
data class Cargo(
    override val platform: TargetPlatform? = null,
    val pkg: String? = null,
    val options: CargoOptions = CargoOptions(),
    val arch: Map<String, CargoArchSpecific> = mapOf(),
    val parameters: List<String> = listOf(),
    val rustFlags: List<String> = listOf(),
    val outName: String? = null,
    val profile: String? = null,
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    val emits: RustEmitType = RustEmitType.StaticLib,
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsPlatforms, SupportsToolchains {

    private val effectivePlatform by delegatedEffectivePlatform()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)
    private val triple by lazy {
        (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
            .targetTriple
    }
    private val effectiveOutName by lazy {
        outName ?: emits.transformModuleName(module.name)
    }
    private val effectiveProfile by lazy {
        profile ?: defaults<Cargo>().firstNotNullOfOrNull { it.profile } ?: "release"
    }
    private val profileDirName by lazy {
        when (effectiveProfile) {
            "dev" -> "debug"
            else -> effectiveProfile
        }
    }

    override val outType = OutType.Intermediary

    private fun combinedParameters(): List<String> =
        (parameters +
                (arch[context.targetDevice.fullArch]?.parameters ?: listOf()) +
                (arch[context.targetDevice.arch]?.parameters ?: listOf()) +
                defaults<Cargo>().flatMap { it.combinedParameters() })

    private fun combinedUnstableOptions(): Map<String, String?> =
        defaults<Cargo>().mapFlattened { it.combinedUnstableOptions() } + options.unstable.mergedOptions

    private fun combinedConfigs(): Map<String, String> =
        defaults<Cargo>().mapFlattened { it.combinedConfigs() } + mapOf(
            "build.dep-info-basedir" to "\"${context.baseRelativePath(context.baseDir)}\"",
        ) + options.configuration

    private fun ownRustFlags(): List<String> =
        rustFlags +
            (arch[context.targetDevice.fullArch]?.rustFlags ?: listOf()) +
            (arch[context.targetDevice.arch]?.rustFlags ?: listOf()) +
            defaults<Cargo>().flatMap { it.ownRustFlags() }

    private fun combinedRustFlags(): List<String> = tc.rustFlagsFor(effectivePlatform) + ownRustFlags()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [Cargo] \$out",
            depFile = (outputDir / "$effectiveOutName.d").toString(),
            command = tc.cargoBuild(
                targetTriple = triple,
                targetDir = outputDirFor(this@Cargo).toString().shellEscaped,
                manifestPath = "\$in",
                pkg = (pkg ?: defaults<Cargo>().firstNotNullOfOrNull { it.pkg })?.shellEscaped,
                profile = effectiveProfile,
                configs = combinedConfigs(),
                unstableOptions = combinedUnstableOptions(),
                additionalParameters = combinedParameters().map { it.shellEscaped },
                rustFlags = combinedRustFlags(),
                nightly =
                options.nightly ?: defaults<Cargo>().firstNotNullOfOrNull { it.options.nightly } ?: false ||
                combinedUnstableOptions().isNotEmpty(),
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = findInputFiles("Cargo.toml")

    private val outputDir get() = outputDirFor(this@Cargo) / triple.toString() / profileDirName

    override fun ninjaInstructions() = listOf(
        NinjaBuildInstruction(
            outputs = listOf((outputDir / "$effectiveOutName.${emits.fileExt}").toString()),
            ruleName = ruleName,
            inputs = inputFiles().map { cSourcePath -> context.baseRelativePath(cSourcePath).toString() }
        )
    )

}

@Serializable
data class CargoArchSpecific(
    val parameters: List<String> = listOf(),
    val rustFlags: List<String> = listOf(),
)

@Serializable
data class CargoOptions(
    val nightly: Boolean? = null,
    val configuration: Map<String, String> = mapOf(),
    val unstable: CargoUnstableOptions = CargoUnstableOptions(),
)

@Serializable
data class CargoUnstableOptions(
    val buildStd: List<String> = listOf(),
    val buildStdFeatures: List<String> = listOf(),
    val unstableOptions: Boolean = false,
    val other: List<String> = listOf()
) {
    val mergedOptions by lazy {
        mapOf(
            "build-std" to buildStd.joinToString(","),
            "build-std-features" to buildStdFeatures.joinToString(",")
        ).filter { (_, v) -> v.isNotEmpty() } +
                other.associate { it.splitPadded("=", max = 2) { "" }.asPair() }
    }
}
