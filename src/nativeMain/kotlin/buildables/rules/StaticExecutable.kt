package buildables.rules

import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.applyFilters
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.asNinjaCommand
import okio.Path
import okio.Path.Companion.toPath
import toolchains.LLVM
import toolchains.types.SupportsELFLinking

private val supportedToolchains = mapOf<String, SupportsELFLinking>(
    "llvm" to LLVM,
)

@Serializable
data class StaticExecutable(
    override val platform: TargetPlatform? = null,
    val objectsFrom: List<String> = listOf(),
    val staticLibsFrom: List<String> = listOf(),
    val arch: Map<String, StaticExecutableArchSpecific> = mapOf(),
    val options: StaticExecutableOptions = StaticExecutableOptions(),
    val flags: List<String> = listOf(),
    val linkerScript: String? = null,
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsPlatforms, SupportsToolchains {

    override val outType = OutType.Product
    private val effectivePlatform by delegatedEffectivePlatform()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    private val effectiveLinkerScript: String? get() =
        linkerScript ?: defaults<StaticExecutable>().firstNotNullOfOrNull { it.run { effectiveLinkerScript } }

    private fun combinedObjectsFrom(): List<String> =
        objectsFrom + defaults<StaticExecutable>().flatMap { it.combinedObjectsFrom() }

    private fun combinedStaticLibsFrom(): List<String> =
        staticLibsFrom + defaults<StaticExecutable>().flatMap { it.combinedStaticLibsFrom() }

    private fun linkerScriptPath() =
        effectiveLinkerScript?.applyFilters(context)?.let {
            context.baseRelativePath(module.directory / it).toString()
        }

    private fun combinedFlags(): List<String> =
        (tc.ldFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.ldFlags ?: listOf()) +
                (arch[context.targetDevice.arch]?.ldFlags ?: listOf()) +
                defaults<StaticExecutable>().flatMap { it.combinedFlags() }).distinct()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [Static Executable] \$out",
            command = tc.link(
                inputFiles = listOf("\$in"),
                outputFile = "\$out",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                optimizationLevel = options.optimize.ifEmpty {
                    defaults<StaticExecutable>().firstNotNullOfOrNull { it.options.optimize.ifEmpty { null } } ?: "1"
                },
                additionalFlags = combinedFlags(),
                linkerScript = linkerScriptPath(),
                useLD = "lld"
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> =
        ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }

    override fun inputFiles() = combinedObjectsFrom().flatMap { objectsFrom ->
        context.rules[objectsFrom]?.outputFiles()
            ?: error("Getting objects: $objectsFrom not defined yet. Only ${context.rules.keys} are defined.")
    } + combinedStaticLibsFrom().flatMap { staticLibsFrom ->
        context.rules[staticLibsFrom]?.outputFiles()
            ?: error("Getting static libs: $staticLibsFrom not defined yet. Only ${context.rules.keys} are defined.")
    }

    override fun ninjaInstructions() =
        listOf(NinjaBuildInstruction(
            outputs = listOf((outputDirFor(this@StaticExecutable) / module.name).toString()),
            ruleName = ruleName,
            inputs = inputFiles().map { it.toString() },
            implicitInputs = listOfNotNull(linkerScriptPath())
        ))

}

@Serializable
data class StaticExecutableArchSpecific(
    @SerialName("ldflags")
    val ldFlags: List<String>
)

@Serializable
data class StaticExecutableOptions(
    val optimize: String = ""
)