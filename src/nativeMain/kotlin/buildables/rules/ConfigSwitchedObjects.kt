package buildables.rules

import kotlinx.serialization.Serializable
import buildables.Rule
import buildables.RuleDefaults
import buildables.context

@Serializable
data class ConfigSwitchedObjects(
    val objects: Map<String, String>,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults() {

    override fun inputFiles() =
        objects
            .filter { (switch, _) -> context.switches[switch] ?: false }
            .map { (switch, ruleName) ->
                context.rules[ruleName] ?: error("Rule $ruleName not found (activated by switch $switch)")
            }
            .flatMap { it.outputFiles() }

    override fun outputFiles() = inputFiles()

}
