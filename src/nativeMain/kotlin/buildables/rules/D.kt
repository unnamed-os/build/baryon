package buildables.rules

import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsSources
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveSrc
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.inputFilesCommon
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.asNinjaCommand
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.LDC
import toolchains.types.SupportsDCompiling

private val supportedToolchains = mapOf<String, SupportsDCompiling>(
    "ldc" to LDC,
)

@Serializable
data class D(
    override val src: List<String>? = null,
    override val platform: TargetPlatform? = null,
    val options: DOptions = DOptions(),
    val arch: Map<String, DArchSpecific> = mapOf(),
    val flags: List<String> = listOf(),
    val includes: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsToolchains, SupportsSources, SupportsPlatforms {

    private val effectivePlatform by delegatedEffectivePlatform()
    override val effectiveSrc by delegatedEffectiveSrc()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    override val outType = OutType.Intermediary

    private fun combinedFlags(): List<String> =
        (tc.dFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.dFlags ?: listOf()) +
                (arch[context.targetDevice.arch]?.dFlags ?: listOf()) +
                defaults<D>().flatMap { it.combinedFlags() })
                    .distinct()

    private fun combinedIncludes(): List<String> =
        includes + defaults<D>().flatMap { it.combinedIncludes() }.distinct()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [D] \$out",
            depFile = "\$out.d",
            command = tc.compileD(
                sourceFile = "\$in",
                outputFile = "\$out",
                depFile = "\$out.d",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                includeDirectories = combinedIncludes(),
                optimizationLevel = options.optimize.ifEmpty {
                    defaults<D>().firstNotNullOfOrNull { it.options.optimize.ifEmpty { null } } ?: "2"
                },
                additionalFlags = combinedFlags(),
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = inputFilesCommon(".d")

    override fun ninjaInstructions() =
        inputFiles().map { cSourcePath ->
            NinjaBuildInstruction(
                outputs = listOf(
                    (outputDirFor(this@D) / module.relativePath(cSourcePath)).replaceExtension("o").toString()
                ),
                ruleName = ruleName,
                inputs = listOf(context.baseRelativePath(cSourcePath).toString())
            )
        }
}

@Serializable
data class DArchSpecific(
    @SerialName("dflags")
    val dFlags: List<String> = listOf()
)

@Serializable
data class DOptions(
    val optimize: String = "",
)
