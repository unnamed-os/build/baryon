package buildables.rules

import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsSources
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveSrc
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.inputFilesCommon
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.asNinjaCommand
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.ZigCompiler
import toolchains.types.SupportsZigCompiling

private val supportedToolchains = mapOf<String, SupportsZigCompiling>(
    "zig" to ZigCompiler,
)

@Serializable
data class Zig(
    override val src: List<String>? = null,
    override val platform: TargetPlatform? = null,
    val options: ZigOptions = ZigOptions(),
    val arch: Map<String, ZigArchSpecific> = mapOf(),
    val defines: Map<String, String> = mapOf(),
    val flags: List<String> = listOf(),
    val includes: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsToolchains, SupportsSources, SupportsPlatforms {


    private val effectivePlatform by delegatedEffectivePlatform()
    override val effectiveSrc by delegatedEffectiveSrc()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    override val outType = OutType.Intermediary

    private fun combinedFlags(): List<String> =
        (tc.zigFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.flags ?: listOf()) +
                (arch[context.targetDevice.arch]?.flags ?: listOf()) +
                defines.map { (k, v) -> "-D$k=$v" } +
                defaults<Zig>().flatMap { it.combinedFlags() })
                    .distinct()

    private fun combinedIncludes(): List<String> =
        includes + defaults<Zig>().flatMap { it.combinedIncludes() }.distinct()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [Zig] \$out",
            depFile = "\$out.d",
            command = tc.compileZig(
                sourceFile = "\$in",
                outputFile = "\$out",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                includeDirectories = combinedIncludes(),
                optimizationLevel = options.optimize ?:
                    defaults<Zig>().firstNotNullOfOrNull { it.options.optimize } ?: "ReleaseFast",
                additionalFlags = combinedFlags(),
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = inputFilesCommon(".zig")

    override fun ninjaInstructions() =
        inputFiles().map { cSourcePath ->
            NinjaBuildInstruction(
                outputs = listOf(
                    (outputDirFor(this@Zig) / module.relativePath(cSourcePath))
                        .replaceExtension("ll").toString()
                ),
                ruleName = ruleName,
                inputs = listOf(context.baseRelativePath(cSourcePath).toString())
            )
        }
}

@Serializable
data class ZigArchSpecific(
    val flags: List<String> = listOf()
)

@Serializable
data class ZigOptions(
    val optimize: String? = null,
)
