package buildables.rules

import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import generator.ninja.asNinjaCommand
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.LLVM
import toolchains.types.SupportsLLVMIRCompiling

private val supportedToolchains = mapOf<String, SupportsLLVMIRCompiling>(
    "llvm" to LLVM,
)

@Serializable
data class LLVMIR(
    override val platform: TargetPlatform? = null,
    val irFrom: List<String> = listOf(),
    val arch: Map<String, LLVMIRArchSpecific> = mapOf(),
    val options: LLVMIROptions = LLVMIROptions(),
    val flags: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsToolchains, SupportsPlatforms {

    private val effectivePlatform by delegatedEffectivePlatform()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    private fun combinedFlags(): List<String> =
        (tc.llvmIRFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.flags ?: listOf()) +
                (arch[context.targetDevice.arch]?.flags ?: listOf()) +
                defaults<LLVMIR>().flatMap { it.combinedFlags() }).distinct()

    override fun ninjaRule() = NinjaRule(
        name = ruleName,
        description = "$ruleName [LLVM IR] \$out",
        command = tc.compileLLVMIR(
            sourceFile = "\$in",
            outputFile = "\$out",
            targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                .targetTriple,
            optimizationLevel = options.optimize.ifEmpty {
                defaults<LLVMIR>().firstNotNullOfOrNull { it.options.optimize.ifEmpty { null } } ?: "2"
            },
            additionalFlags = combinedFlags(),
        ).asNinjaCommand
    )

    private fun combinedIRFrom(): List<String> =
        irFrom + defaults<LLVMIR>().flatMap { it.combinedIRFrom() }

    override fun outputFiles(): List<Path> =
        ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }

    private fun ruleInputFiles() = combinedIRFrom().associate { objectsFrom ->
        context.rules[objectsFrom]?.let { it to it.outputFiles() }
            ?: error("Getting LLVM IR files: $objectsFrom not defined yet. Only ${context.rules.keys} are defined.")
    }

    override fun inputFiles() = ruleInputFiles().values.flatten()

    override fun ninjaInstructions() =
        ruleInputFiles().flatMap { (_, irPaths) ->
            irPaths.map { irPath ->
                NinjaBuildInstruction(
                    outputs = listOf(
                        (outputDirFor(this@LLVMIR) / irPath)
                            .replaceExtension("o").toString()
                    ),
                    ruleName = ruleName,
                    inputs = listOf(irPath.toString()),
                )
            }
        }
}


@Serializable
data class LLVMIRArchSpecific(
    val flags: List<String> = listOf()
)

@Serializable
data class LLVMIROptions(
    val optimize: String = "",
)
