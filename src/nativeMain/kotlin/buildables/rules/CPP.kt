package buildables.rules

import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import buildables.OutType
import buildables.Rule
import buildables.RuleDefaults
import buildables.SupportsPlatforms
import buildables.SupportsSources
import buildables.SupportsToolchains
import buildables.TargetPlatform
import buildables.context
import buildables.defaults
import buildables.delegatedEffectivePlatform
import buildables.delegatedEffectiveSrc
import buildables.delegatedEffectiveToolchain
import buildables.delegatedEffectiveToolchainName
import buildables.inputFilesCommon
import buildables.outputDirFor
import buildables.ruleName
import generator.ninja.asNinjaCommand
import okio.Path
import okio.Path.Companion.toPath
import replaceExtension
import toolchains.LLVM
import toolchains.types.SupportsCCompiling
import toolchains.types.SupportsCPPCompiling

private val supportedToolchains = mapOf<String, SupportsCPPCompiling>(
    "llvm" to LLVM,
)

@Serializable
data class CPP(
    override val src: List<String>? = null,
    override val platform: TargetPlatform? = null,
    val options: CPPOptions = CPPOptions(),
    val arch: Map<String, CPPArchSpecific> = mapOf(),
    val defines: Map<String, String> = mapOf(),
    val flags: List<String> = listOf(),
    val includes: List<String> = listOf(),
    override val toolchain: String? = null,
    override val defaults: List<String> = listOf(),
    override val dependsOn: List<String> = listOf(),
) : Rule by RuleDefaults(), SupportsToolchains, SupportsSources, SupportsPlatforms {

    private val effectivePlatform by delegatedEffectivePlatform()
    override val effectiveSrc by delegatedEffectiveSrc()
    private val tcName by delegatedEffectiveToolchainName(supportedToolchains)
    private val tc by delegatedEffectiveToolchain(supportedToolchains)

    override val outType = OutType.Intermediary

    private fun combinedFlags(): List<String> =
        (tc.cppFlagsFor(effectivePlatform) +
                flags +
                (arch[context.targetDevice.fullArch]?.cFlags ?: listOf()) +
                (arch[context.targetDevice.arch]?.cFlags ?: listOf()) +
                defines.map { (k, v) -> "-D$k=$v" } +
                defaults<CPP>().flatMap { it.combinedFlags() })
                    .distinct()

    private fun combinedIncludes(): List<String> =
        includes + defaults<CPP>().flatMap { it.combinedIncludes() }.distinct()

    override fun ninjaRule() =
        NinjaRule(
            name = ruleName,
            description = "$ruleName [C++] \$out",
            depFile = "\$out.d",
            command = tc.compileCPP(
                sourceFile = "\$in",
                outputFile = "\$out",
                depFile = "\$out.d",
                targetTriple = (context.targetDevice.toolchains[tcName] ?: error("missing toolchain config $tcName"))
                    .targetTriple,
                includeDirectories = combinedIncludes(),
                optimizationLevel = options.optimize.ifEmpty {
                    defaults<CPP>().firstNotNullOfOrNull { it.options.optimize.ifEmpty { null } } ?: "2"
                },
                additionalFlags = combinedFlags(),
                cppStandard = options.std.ifEmpty {
                    defaults<CPP>().firstNotNullOfOrNull { it.options.std.ifEmpty { null } } ?:
                        error("Please specify std in CPP rule")
                },
            ).asNinjaCommand
        )

    override fun outputFiles(): List<Path> {
        return ninjaInstructions().flatMap { it.outputs.map { out -> out.toPath() } }
    }

    override fun inputFiles() = inputFilesCommon(".cpp", ".cxx")

    override fun ninjaInstructions() =
        inputFiles().map { cSourcePath ->
            NinjaBuildInstruction(
                outputs = listOf(
                    (outputDirFor(this@CPP) / module.relativePath(cSourcePath)).replaceExtension("o").toString()
                ),
                ruleName = ruleName,
                inputs = listOf(context.baseRelativePath(cSourcePath).toString())
            )
        }
}

@Serializable
data class CPPArchSpecific(
    @SerialName("cflags")
    val cFlags: List<String> = listOf()
)

@Serializable
data class CPPOptions(
    val optimize: String = "",
    val std: String = "",
)
