package buildables

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import okio.Path

@Serializable
data class Configuration(
    val arch: String? = null,
    val switches: Map<String, Boolean> = mapOf(),
) {
    @Transient
    lateinit var directory: Path

    @Transient
    lateinit var definitionPath: Path
}
