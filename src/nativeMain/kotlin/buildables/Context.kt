package buildables

import okio.Path

data class Context(
    val baseDir: Path,
    val outDir: Path,
    val targetDevice: TargetDevice,
) {
    val modules = mutableMapOf<String, Module>()
    val rules = mutableMapOf<String, Rule>()
    val defaults = mutableMapOf<String, Defaults>()
    val configs = mutableListOf<Configuration>()

    val switches get() = configs.flatMap { it.switches.toList() }.toMap()

    fun baseRelativePath(path: Path) = path.relativeTo(baseDir)
    inline fun <reified T : Rule> rulesSortedByHierarchy() =
        modules.values.sortedBy { it.directory.segments.size }.mapNotNull { it.rules.ruleOfType<T>() }
    inline fun <reified T : Rule> rulesSortedByDescendingHierarchy() =
        modules.values.sortedByDescending { it.directory.segments.size }.mapNotNull { it.rules.ruleOfType<T>() }
    fun allRulesSortedByDescendingHierarchy() =
        modules.values.sortedByDescending { it.directory.segments.size }.flatMap { it.rules.allRules }
}
