package buildables

import buildables.rules.ASM
import buildables.rules.C
import buildables.rules.CPP
import buildables.rules.Cargo
import buildables.rules.ConfigSwitchedObjects
import buildables.rules.D
import buildables.rules.Download
import buildables.rules.LLVMIR
import buildables.rules.Rust
import buildables.rules.StaticExecutable
import buildables.rules.Swift
import buildables.rules.Unzip
import buildables.rules.Zig
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import okio.Path

inline fun <reified T : Rule> T.translateTypeToRuleKey() = when (this) {
    is StaticExecutable -> "static-exec"
    is ConfigSwitchedObjects -> "config-switched"
    is LLVMIR -> "llvm-ir"
    else -> this::class.simpleName!!.lowercase()
}

@Serializable
data class Rules(
    val c: C? = null,
    val d: D? = null,
    val asm: ASM? = null,
    val cpp: CPP? = null,
    val zig: Zig? = null,
    val rust: Rust? = null,
    val cargo: Cargo? = null,
    val swift: Swift? = null,
    @SerialName("static-exec")
    val staticExecutable: StaticExecutable? = null,
    @SerialName("llvm-ir")
    val llvmIR: LLVMIR? = null,
    @SerialName("config-switched")
    val configSwitched: ConfigSwitchedObjects? = null,
    val download: Download? = null,
    val unzip: Unzip? = null,
) {
    @Transient
    val allRules = listOfNotNull(
        c,
        d,
        asm,
        cpp,
        zig,
        rust,
        cargo,
        swift,
        staticExecutable,
        llvmIR,
        configSwitched,
        download,
        unzip,
    )

    fun ninjaRules(context: Context) = context.run {
        allRules.mapNotNull { rule ->
            rule.ninjaRule().also {
                context.rules[rule.ruleName] = rule
            }
        }
    }

    fun ninjaInstructions(context: Context) = context.run {
        allRules.flatMap { rule -> rule.ninjaInstructions().map {
            it.copy(implicitInputs = it.implicitInputs + rule.dependsOn.flatMap { dependency ->
                context.rules[dependency]?.outputFiles()?.map { out -> out.toString() } ?:
                    error("Dependency $dependency does not exist. Choose one of ${context.rules.keys}")
            })
        } }
    }

    inline fun <reified T : Rule> ruleOfType() = allRules.find { it is T } as T?
}

enum class OutType(val dirName: String) {
    Intermediary("intermediaries"),
    Product("product")
}

interface Rule {
    fun ninjaRule(): NinjaRule?
    fun ninjaInstructions(): List<NinjaBuildInstruction>
    fun inputFiles(): List<Path>
    fun outputFiles(): List<Path>

    val outType: OutType

    @Transient
    var module: ModuleLike

    val defaults: List<String>

    val dependsOn: List<String>
}

interface SupportsPlatforms {
    val platform: TargetPlatform?
}

interface SupportsToolchains {
    val toolchain: String?
}

interface SupportsSources {
    val src: List<String>?
    val effectiveSrc: List<String>
}
