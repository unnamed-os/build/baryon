package buildables

fun String.applyFilters(context: Context) =
    replace("{arch}", context.targetDevice.arch)
        .replace("{full_arch}", context.targetDevice.fullArch)

val String.shellEscaped get() =
    replace("""\""", """\\""")
        .replace(""""""", """\"""")
        .replace("'", """\'""")
        .replace(" ", """\ """)
        .replace("?", """\?""")
        .replace("&", """\&""")
        .replace("|", """\|""")
        .replace("$", """\$""")
        .replace("!", """\!""")
        .replace("(", """\(""")
        .replace(")", """\)""")
        .replace("{", """\{""")
        .replace("}", """\}""")
        .replace("[", """\[""")
        .replace("]", """\]""")
        .replace("`", """\`""")
