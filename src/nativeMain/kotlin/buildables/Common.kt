package buildables

import fs
import generator.ninja.NinjaBuildInstruction
import generator.ninja.NinjaRule
import isDirectory
import isRegularFile
import okio.Path
import okio.Path.Companion.toPath
import startsWith
import toolchains.fromToolchains
import toolchains.types.Toolchain


class RuleDefaults : Rule {
    override lateinit var module: ModuleLike
    override fun ninjaRule(): NinjaRule? = null
    override fun ninjaInstructions() = listOf<NinjaBuildInstruction>()
    override fun inputFiles() = listOf<Path>()
    override fun outputFiles() = listOf<Path>()
    override val outType = OutType.Intermediary
    override val defaults: List<String> = listOf()
    override val dependsOn: List<String> = listOf()
}

inline fun <reified T> T.delegatedEffectivePlatform() where T : Rule, T: SupportsPlatforms =
    lazy {
        platform ?: defaults<T>().firstNotNullOfOrNull { it.platform }
        ?: error("Please specify platform. Available platforms: ${TargetPlatform.values().joinToString()}")
    }

inline fun <reified T, reified TC : Toolchain> T.delegatedEffectiveToolchainName(
    supportedToolchains: Map<String, TC>
) where T : Rule, T: SupportsToolchains =
    lazy {
        (toolchain ?: defaults<T>().firstNotNullOfOrNull { it.toolchain } ?: supportedToolchains.keys.first())
    }


inline fun <reified T, reified TC : Toolchain> T.delegatedEffectiveToolchain(
    supportedToolchains: Map<String, TC>
) where T : Rule, T: SupportsToolchains =
    lazy {
        (toolchain ?: defaults<T>().firstNotNullOfOrNull { it.toolchain } ?: supportedToolchains.keys.first())
            .fromToolchains(supportedToolchains)
    }

inline fun <reified T> T.delegatedEffectiveSrc() where T : Rule, T : SupportsSources =
    lazy {
        src ?: defaults<T>().firstNotNullOfOrNull { it.src } ?:
        error("Please specify src")
    }

inline fun <reified T> T.inputFilesCommon(vararg fileSuffixes: String) where T : Rule, T : SupportsSources =
    effectiveSrc.flatMap { src ->
        (module.directory / src.toPath()).normalized().let { srcPath ->
            if (srcPath.isDirectory) {
                fs.listRecursively(srcPath, followSymlinks = false)
                    .filter {
                        fileSuffixes.isEmpty() ||
                                fileSuffixes.any { fileSuffix -> it.name.lowercase().endsWith(fileSuffix) }
                    }
                    .map { it.normalized() }
                    .filterNot { inputFileExistsFurtherDown(it) }
                    .toList()
            } else if (srcPath.isRegularFile) {
                listOf(srcPath)
            } else error("Collecting input files: $srcPath does not exist or is invalid")
        }
    }

inline fun <reified T : Rule> T.findInputFiles(vararg fileNames: String, recursive: Boolean = false) =
    module.directory.normalized().let { dir ->
        (if (recursive) fs.listRecursively(dir) else fs.list(dir).asSequence())
            .filter { it.name in fileNames }
            .toList()
            .also { paths ->
                if (paths.size < fileNames.size) {
                    error("finding input files: ${fileNames.toSet() - paths.map { it.name }.toSet()} not found")
                } else if (paths.size > fileNames.size) {
                    error("finding input files: too many results: ${paths.map { it.toString() }}")
                }
            }
    }


inline val Rule.context get() = module.context

inline fun <reified T : Rule> Rule.defaults() =
    defaults.map { defaultsName ->
        module.context.defaults[defaultsName]?.rules?.ruleOfType<T>() ?:
        error("There is no defaults of type ${T::class.simpleName} with name $defaultsName")
    }

inline val Rule.ruleName get() = "${module.name}.${translateTypeToRuleKey()}"

fun outputDirFor(rule: Rule) =
    (rule.module.context.outDir / rule.outType.dirName / rule.module.name / rule::class.simpleName!!.lowercase())


inline fun <reified T : Rule> Rule.inputFilesOf() =
    module.context.modules.values.flatMap { it.inputFilesOf<T>() }
inline fun <reified T : Rule> Rule.inputFilesOfExcept(cond: (Module) -> Boolean) =
    module.context.modules.values.filterNot(cond).flatMap { it.inputFilesOf<T>() }

inline fun <reified T : Rule> T.inputFileExistsFurtherDown(inputFile: Path) =
    context.allRulesSortedByDescendingHierarchy()
        .filterNot { it.module.name == module.name }
        .filter { it.module.directory.segments.size > module.directory.segments.size }
        .filter { it.module.directory.startsWith(module.directory) }
        .any { inputFile.normalized() in it.inputFiles() }
