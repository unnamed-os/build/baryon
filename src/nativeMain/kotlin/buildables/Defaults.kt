package buildables

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import okio.Path

@Serializable
data class Defaults(
    override val name: String,
    val arch: String? = null,
    val rules: Rules = Rules(),
) : ModuleLike {
    @Transient
    override lateinit var context: Context

    @Transient
    override lateinit var directory: Path

    @Transient
    lateinit var definitionPath: Path
}
