package generator.ninja

class NinjaRoot : Snippet {
    private val values = mutableMapOf<String, Any>()
    private val rules = mutableListOf<NinjaRule>()
    private val instructions = mutableListOf<NinjaBuildInstruction>()
    private val defaults = mutableListOf<NinjaDefault>()

    override fun toString() =
        listOf(
            values.map { (key, value) ->
                listOf<Snippet>(key.snippet, value.toString().snippet).equalsCombined
            }.newLineSeparated,
            emptySnippet,
            rules.newLineSeparated,
            instructions.newLineSeparated,
            emptySnippet,
            defaults.newLineSeparated,
            emptySnippet
        ).newLineSeparated.toString()

    operator fun set(key: String, value: Any) {
        values[key] = value
    }

    operator fun NinjaRule.unaryPlus() {
        rules += this
    }

    operator fun NinjaBuildInstruction.unaryPlus() {
        instructions += this
    }

    operator fun NinjaDefault.unaryPlus() {
        this@NinjaRoot.defaults += this
    }

    operator fun Iterable<NinjaRule>.unaryPlus() {
        rules.addAll(this)
    }

    operator fun Iterable<NinjaBuildInstruction>.unaryPlus() {
        instructions.addAll(this )
    }

    operator fun Iterable<NinjaDefault>.unaryPlus() {
        defaults.addAll(this)
    }

}