package generator.ninja

class NinjaGenerator {

    val requiredVersion = "1.10"
    var buildDir = "out"
    val defaults = mutableListOf<String>()

    private val root = NinjaRoot()

    operator fun invoke(content: NinjaRoot.() -> Unit) {
        root.run(content)
    }

    override fun toString(): String {
        root["ninja_required_version"] = requiredVersion
        root["builddir"] = buildDir
        return root.toString()
    }

    fun applyInRoot(block: NinjaRoot.() -> Unit) = apply { root.apply { block() } }

}

fun ninja(generatorBlock: NinjaGenerator.() -> Unit = {}, content: NinjaRoot.() -> Unit) =
    NinjaGenerator()
        .apply(generatorBlock)
        .apply { this(content) }
        .apply {
            if (defaults.isNotEmpty()) {
                applyInRoot {
                    +NinjaDefault(defaults)
                }
            }
        }
