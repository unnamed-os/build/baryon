package generator.ninja

class NinjaRule(
    val name: String,
    val description: String,
    val command: String,
    val depFile: String? = null,
) : Snippet {

    override fun toString() = listOf(
        listOf("rule".snippet, name.snippet).spaced,
        wrap {
            +listOf("description".snippet, description.snippet).equalsCombined
            +listOf("command".snippet, command.snippet).equalsCombined
            depFile?.let {
                +listOf("depfile".snippet, it.snippet).equalsCombined
            }
        },
        emptySnippet,
    ).newLineSeparated.toString()
}