package generator.ninja

const val defaultIndentation = 2

interface Snippet {
    override fun toString(): String
}

open class StringStringer(val generator: () -> String) : Snippet {
    override fun toString() = generator()
}

class EnclosedCodeSnippet(private val snippet: Snippet) : Snippet {
    override fun toString() = "($snippet)"
}

val Char.snippet get() = StringStringer { "$this" }
val String.snippet get() = StringStringer { this }
val Snippet.enclosed get() = EnclosedCodeSnippet(this)
val List<Snippet>.commaCombined get() = StringStringer { joinToString(", ") }
val List<Snippet>.combined get() = StringStringer { joinToString("") }
val List<Snippet>.spaced get() =
    StringStringer { map { it.toString() }.filter { it.isNotBlank() }.joinToString(" ") }
fun List<Snippet>.combinedUsing(chars: CharSequence) = StringStringer { joinToString(chars) }
fun List<Snippet>.combinedUsing(snippet: Snippet) = StringStringer { joinToString(snippet.toString()) }.toString()
val List<Snippet>.equalsCombined get() = StringStringer { joinToString(" = ") }
val Snippet.padded get() = " $this "
val Snippet.angleBracketed get() = listOf("<".snippet, this, ">".snippet).combined
val Snippet.bracketed get() = listOf("[".snippet, this, "]".snippet).combined
val Snippet.curlyEnclosed get() = listOf("{".snippet, this, "}".snippet).combined
val Snippet.quoted get() = listOf('"'.snippet, this, '"'.snippet).combined
val List<Snippet>.newLineSeparated get() = StringStringer { joinToString("\n") }
fun Snippet.suffixed(chars: CharSequence) = StringStringer { toString() + chars }
fun Snippet.prefixed(chars: String) = StringStringer { chars + toString() }
val emptySnippet = "".snippet

operator fun Snippet.plus(other: Snippet) = StringStringer { toString() + other.toString() }
operator fun Snippet.plus(other: String) = StringStringer { toString() + other }

abstract class SnippetWrapCapable : Snippet {

    private val snippets = mutableListOf<Snippet>()
    val stringed by lazy {
        wrap()
        asString
    }

    operator fun Snippet.unaryPlus() {
        add(this@unaryPlus)
    }

    fun add(snippet: Snippet) {
        snippets += snippet
    }

    abstract fun wrap()

    private val asString get() = snippets.joinToString("\n") { "${" ".repeat(defaultIndentation)} $it" }

    override fun toString() = stringed
}

class SnippetWrapper(val content: SnippetWrapper.() -> Unit) : SnippetWrapCapable() {

    override fun wrap() {
        content()
    }

}

inline fun wrap(crossinline content: SnippetWrapper.() -> Unit) = SnippetWrapper { content() }
