package generator.ninja

val Iterable<String>.asNinjaCommand get() = joinToString(" ")
