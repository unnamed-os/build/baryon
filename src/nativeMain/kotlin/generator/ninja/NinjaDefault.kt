package generator.ninja

class NinjaDefault(
    val defaults: List<String>,
) : Snippet {

    override fun toString() = listOf(
        "default".snippet,
        *defaults.map { it.snippet }.toTypedArray()
    ).spaced.toString()

}


