package generator.ninja

data class NinjaBuildInstruction(
    val outputs: List<String>,
    val implicitOutputs: List<String> = listOf(),
    val ruleName: String,
    val inputs: List<String>,
    val implicitInputs: List<String> = listOf(),
) : Snippet {

    override fun toString() = listOf(
        "build".snippet,
        listOf(
            outputs.map { it.snippet }.spaced,
            implicitOutputs.let { outputs ->
                if (outputs.isEmpty()) "".snippet else outputs.map { it.snippet }.spaced.prefixed("| ")
            },
        ).spaced.suffixed(":"),
        ruleName.snippet,
        listOf(
            inputs.map { it.snippet }.spaced,
            implicitInputs.let { inputs ->
                if (inputs.isEmpty()) "".snippet else inputs.map { it.snippet }.spaced.prefixed("| ")
            },
        ).spaced,
    ).spaced.toString()

}


