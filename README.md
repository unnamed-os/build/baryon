# baryon

Ninja Build File Generator

Uses declarative files to generate a ninja build file

This is the successor to gonani.
Due to the complexity in the hard-to-read code of gonani,
this is being rewritten in Kotlin.
To avoid the JVM, this is a Kotlin/Native project.

## License

This program and all of its source code is free software and licensed under the GPLv3.

```
    baryon Ninja Build File Generator
    Copyright (C) 2023  Simão Gomes Viana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
